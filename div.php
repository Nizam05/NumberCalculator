<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 3/16/2018
 * Time: 12:29 PM
 */

include_once "vendor/autoload.php";

use Pondit\Calculator\NumberCalculator\Division;
use Pondit\Calculator\NumberCalculator\Displayer;

$divide1=new Division();

$displayer1=new Displayer();

$result="The division of three number is:".$divide1->dividethreenumber($_POST['numberone'],$_POST['numbertwo'],$_POST['numberthree']);
$displayer1 ->displaypre($result);
$displayer1 ->displayH1($result);
$displayer1 ->displaysimple($result);