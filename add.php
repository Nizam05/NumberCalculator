<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 3/20/2018
 * Time: 10:04 PM
 */
include_once "vendor/autoload.php";

use Pondit\Calculator\NumberCalculator\Addition;
use Pondit\Calculator\NumberCalculator\Displayer;



$addition1=new Addition();
$displayer1=new Displayer();

$result="The  addition of three number is:".$addition1->addthreenumber($_POST['numberone'],$_POST['numbertwo'],$_POST['numberthree']);
$displayer1 ->displaypre($result);
$displayer1 ->displayH1($result);
$displayer1 ->displaysimple($result);